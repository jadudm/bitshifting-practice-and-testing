# http://www.cs.colby.edu/maxwell/courses/tutorials/maketutor/
CC=gcc
CFLAGS=-std=gnu99 -I. 

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

bitshiftingtests: allyourbase.o bitshifting.o main.o
	gcc -o bitshifting_tests allyourbase.o bitshifting.o main.o

all: bitshiftingtests
	echo "Done."
