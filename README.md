# Bitshifting For Fun and Profit

The bitshifting exercise gives you an opportunity to practice working with bitshifting operators and demonstrate your understanding by writing code to test those functions.

## Instructions

1. **Check out the code.** <br>
    First, check out the code. You can do this with git.

    <pre>
    git clone https://jadudm@bitbucket.org/jadudm/bitshifting-practice-and-testing.git
    </pre>
    
    This will create the directory "bitshifting-practice-and-testing" in whatever directory you run the command. It will contain the code you need to get started.
    
1. **Rename the directory.** <br>
    Rename the directory <b>bitshifting-<i>username</i></b>. You can do that with the 'mv' command. This will make it easier for me to know whose code I am working with later.

1. **Read all of the code.** <br>
    I recommend reading it with a partner, and discussing it as you go. If you do not understand something, either attempt to look it up, or discuss it with other classmates. If you still do not understand something, ask the professor. In short, it would be wise to make sure you understand the code you have before you begin.

1. **Implement Functions.** <br>
    After reading the code, implement the functions in bitshifting.c. I recommend implementing these one at a time, and writing the tests for each function as you go.

1. **Test Thoroughly.** <br>
    Thoroughly test each function. Demonstrate the code with a wide range of values. Ideally, these values will demonstrate the limits of the function (for example, 0x00 as well as 0xFF, for 16-bit values) as well as one or more values in-between.

1. **Credit partners.** <br>
    If you worked with others, create a file called **COLLABORATION.txt** in
    the directory. Describe who you worked with (if you did) and to what extent.

## Compiling and Running

You should be able to compile your code by typing

'''
$ make
'''

on the command line. The Makefile will output a successfully compiled program as

'''
bitshifting_tests
'''

which you can run with the command

'''
$ ./bitshifting_tests
'''

## Submission

When you are done, you should extract a .tar.gz file from your C9 area and upload that to Moodle.