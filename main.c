#include "bitshifting.h"
#include "allyourbase.h"

/* This practice has you first defining some values in hex,
 * then defining functions to manipulate those values. You could
 * almost say you're writing some functions and their test harness.
 * 
 * Your functions will all be defined in bitshifting.c.
 *
 * We will be using int16 as the type throughout this code.
 * It is an integer that is limited to 16 bits, even if you 
 * are programming on a 64-bit computer.
 */
 

int main () {
  
  int16 result;
  
  // Here's an example test. Notice how well it is documented. 
  // Your tests should follow suit.

  /* ************************************************************ */
  // Check 1
  
  // DESCRIPTION
  // Tests whether set_one_bit correctly sets the third bit
  // of the number zero. Should yield 8.
  result = set_one_bit (0, 3);
  printf("set_one_bit (0, 3): ");
  base2(result);
  newline();
  
  // Setting the third bit should result in
  // 0b00001000, or 0x08, or the number 8.
  // Check that the result is the same.
  if (result == 0x08) {
    printf ("Check 1: Passed.");
    newline();
  } else {
    failed("Check 1", 0x08, result);
  }
  
  // MEMO: This is a fair bit of code to test one function. Further, it is only
  // one test. You should have several tests for each function, and they should
  // demonstrate a range of values, making sure that your function performs 
  // as expected for a wide range of inputs. 
  // Do this for all of the functions defined in bitshifting.c.
  //
  // If writing additional helper functions to simplify the code you write 
  // here is useful, you may do so.
  
}